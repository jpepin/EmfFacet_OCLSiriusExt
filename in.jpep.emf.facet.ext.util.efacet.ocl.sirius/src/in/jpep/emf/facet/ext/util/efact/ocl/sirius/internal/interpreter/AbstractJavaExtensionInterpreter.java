/**
 * Copyright (c) 2018 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal.interpreter;

import java.util.Collection;

import org.eclipse.sirius.common.tools.api.interpreter.IInterpreter;
import org.eclipse.sirius.common.tools.api.interpreter.JavaExtensionsManager;

public class AbstractJavaExtensionInterpreter
		extends AbstractVariableInterpreter {

	protected JavaExtensionsManager javaExtensions;

	public AbstractJavaExtensionInterpreter() {
		super();
		this.javaExtensions = JavaExtensionsManager.createManagerWithOverride();
	}

	@Override
	public void addImport(String dependency) {
		this.javaExtensions.addImport(dependency);
	}

	@Override
	public void removeImport(String dependency) {
		this.javaExtensions.removeImport(dependency);
	}

	@Override
	public void clearImports() {
		this.javaExtensions.clearImports();
	}

	@Override
	public Collection<String> getImports() {
		return this.javaExtensions.getImports();
	}

	@Override
	public void setProperty(Object key, Object value) {
		/*
		 * This is called by the framework with the FILES key in order to pass
		 * us all the VSM files as a Collection.
		 */
		if (IInterpreter.FILES.equals(key) && value instanceof Collection) {
			@SuppressWarnings("unchecked")
			/*
			 * @SuppressWarnings("unchecked") > jpepin, value send by framework
			 * can't be another type than strings
			 */
			final Collection<String> values = (Collection<String>) value;
			this.javaExtensions.updateScope(values);
		}
	}

	@Override
	public void dispose() {
		super.dispose();
		this.javaExtensions.dispose();
	}

}
