/**
 * Copyright (c) 2018 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal.interpreter;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.eclipse.sirius.common.tools.api.interpreter.IVariableStatusListener;
import org.eclipse.sirius.common.tools.api.interpreter.VariableManager;

public class AbstractVariableInterpreter extends InterpreterAdapter {

	private final VariableManager variablesManager;
	private final Set<IVariableStatusListener> variableStatusListeners = new CopyOnWriteArraySet<IVariableStatusListener>();

	public AbstractVariableInterpreter() {
		this.variablesManager = new VariableManager();
	}

	@Override
	public String getVariablePrefix() {
		// no variable prefix for this interpreter.
		return null;
	}

	@Override
	public void setVariable(final String name, final Object value) {
		this.variablesManager.setVariable(name, value);
	}

	@Override
	public void unSetVariable(final String name) {
		this.variablesManager.unSetVariable(name);
		notifyVariableListeners();
	}

	@Override
	public Object getVariable(final String name) {
		return this.variablesManager.getVariable(name);
	}

	@Override
	public void clearVariables() {
		this.variablesManager.clearVariables();
		notifyVariableListeners();
	}

	@Override
	public Map<String, Object> getVariables() {
		return this.variablesManager.getVariables();
	}

	@Override
	public void addVariableStatusListener(
			final IVariableStatusListener newListener) {
		this.variableStatusListeners.add(newListener);
	}

	@Override
	public void removeVariableStatusListener(IVariableStatusListener listener) {
		this.variableStatusListeners.remove(listener);
	}

	/**
	 * Notifies all of the registered variable status listener of our current
	 * variable status. This will be called internally whenever we change the
	 * variable map.
	 */
	private void notifyVariableListeners() {
		for (IVariableStatusListener variableStatusListener : this.variableStatusListeners) {
			variableStatusListener.notifyChanged(getVariables());
		}
	}

	@Override
	public void dispose() {
		this.variablesManager.clearVariables();
		this.variableStatusListeners.clear();
	}

}
