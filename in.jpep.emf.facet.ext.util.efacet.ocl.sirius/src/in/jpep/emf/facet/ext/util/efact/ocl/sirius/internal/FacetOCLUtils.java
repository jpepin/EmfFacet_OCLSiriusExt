/**
 * Copyright (c) 2018 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.facet.efacet.core.IFacetManager;
import org.eclipse.emf.facet.efacet.core.IFacetManagerFactory;
import org.eclipse.emf.facet.util.efacet.ocl.core.IEFacetOCLEnvironmentFactoryFactory;
import org.eclipse.ocl.EnvironmentFactory;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.expressions.OCLExpression;
import org.eclipse.ocl.helper.OCLHelper;

import in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal.interpreter.FacetOCLInterpreter;

public final class FacetOCLUtils {

	private FacetOCLUtils() {
		// must not be called
	}

	public static OCL<?, EClassifier, EOperation, EStructuralFeature, ?, ?, ?, ?, ?, Constraint, ?, ?> getFacetOCL(
			final EObject context) {
		final EnvironmentFactory<?, EClassifier, EOperation, EStructuralFeature, ?, ?, ?, ?, ?, Constraint, ?, ?> environmentFactory = getEnvironmentFactory(
				context);
		final OCL<?, EClassifier, EOperation, EStructuralFeature, ?, ?, ?, ?, ?, Constraint, ?, ?> ocl = OCL
				.newInstance(environmentFactory);
		ocl.getEvaluationEnvironment();
		return ocl;
	}

	@SuppressWarnings("unchecked")
	/*
	 * @SuppressWarnings("unchecked") > jpepin, imposed by interface
	 */
	public static EnvironmentFactory<?, EClassifier, EOperation, EStructuralFeature, ?, ?, ?, ?, ?, Constraint, ?, ?> getEnvironmentFactory(
			final EObject object) {
		final IFacetManager facetManager = getFacetManager(object);
		return IEFacetOCLEnvironmentFactoryFactory.DEFAULT
				.createEFacetOCLEnvironmentFactory(facetManager);
	}

	public static OCLHelper<EClassifier, EOperation, EStructuralFeature, Constraint> getOclHelper(
			final EObject context,
			final OCL<?, EClassifier, EOperation, EStructuralFeature, ?, ?, ?, ?, ?, Constraint, ?, ?> ocl) {
		final OCLHelper<EClassifier, EOperation, EStructuralFeature, Constraint> helper = ocl
				.createOCLHelper();
		final EClass classifier = context.eClass();
		helper.setContext(classifier);
		helper.setInstanceContext(context);
		return helper;
	}

	public static OCLExpression<EClassifier> getOclExpression(
			final EObject context, final String expression,
			final OCL<?, EClassifier, EOperation, EStructuralFeature, ?, ?, ?, ?, ?, Constraint, ?, ?> ocl)
			throws ParserException {
		final OCLHelper<EClassifier, EOperation, EStructuralFeature, Constraint> oclHelper = getOclHelper(
				context, ocl);
		return oclHelper.createQuery(expression);
	}

	private static IFacetManager getFacetManager(final EObject object) {
		final Resource resource = object.eResource();
		final ResourceSet resourceSet = resource.getResourceSet();
		return IFacetManagerFactory.DEFAULT
				.getOrCreateDefaultFacetManager(resourceSet);
	}

	public static String getExpressionWithoutPrefix(final String expression) {
		return expression.replace(FacetOCLInterpreter.PREFIX, ""); //$NON-NLS-1$
	}

}
