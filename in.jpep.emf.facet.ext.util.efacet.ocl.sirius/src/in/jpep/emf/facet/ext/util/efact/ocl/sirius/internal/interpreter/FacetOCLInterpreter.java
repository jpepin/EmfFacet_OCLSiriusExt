/**
 * Copyright (c) 2018 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal.interpreter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.expressions.OCLExpression;
import org.eclipse.ocl.helper.OCLHelper;
import org.eclipse.sirius.common.tools.api.interpreter.EvaluationException;
import org.eclipse.sirius.common.tools.api.interpreter.IInterpreterContext;
import org.eclipse.sirius.common.tools.api.interpreter.IInterpreterStatus;
import org.eclipse.sirius.common.tools.api.interpreter.InterpreterStatusFactory;

import in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal.FacetOCLUtils;

public class FacetOCLInterpreter extends AbstractJavaExtensionInterpreter {

	public static final String SELF_VARIABLE_NAME = "self"; //$NON-NLS-1$
	public static final String PREFIX = "focl:"; //$NON-NLS-1$ ;

	@Override
	public String getPrefix() {
		return PREFIX;
	}

	@Override
	public boolean provides(String expression) {
		return expression != null && expression.startsWith(getPrefix());
	}

	@Override
	public boolean supportsValidation() {
		return true;
	}

	@Override
	public Collection<IInterpreterStatus> validateExpression(
			final IInterpreterContext context, final String expression) {
		final List<IInterpreterStatus> status = new ArrayList<IInterpreterStatus>();
		final EObject elementContext = context.getElement();
		final OCL<?, EClassifier, EOperation, EStructuralFeature, ?, ?, ?, ?, ?, Constraint, ?, ?> ocl = FacetOCLUtils
				.getFacetOCL(elementContext);
		final OCLHelper<EClassifier, EOperation, EStructuralFeature, Constraint> oclHelper = FacetOCLUtils
				.getOclHelper(elementContext, ocl);
		OCLExpression<EClassifier> oclExpression = null;
		try {
			oclExpression = oclHelper.createQuery(expression);
		} catch (ParserException exc) {
			final IInterpreterStatus excStatus = InterpreterStatusFactory
					.createInterpreterStatus(context, IInterpreterStatus.ERROR,
							exc.getMessage());
			status.add(excStatus);
			final Diagnostic diagnotic = oclHelper.getProblems();
			final String message = diagnotic.getMessage();
			final IInterpreterStatus newStatus = InterpreterStatusFactory
					.createInterpreterStatus(context, IInterpreterStatus.ERROR,
							message);
			status.add(newStatus);
		}
		if (oclExpression != null) {
			final Object result = ocl.evaluate(elementContext, oclExpression);
			if (ocl.isInvalid(result)) {
				final Diagnostic diagnotic = ocl.getEvaluationProblems();
				final String message = diagnotic.getMessage();
				final IInterpreterStatus newStatus = InterpreterStatusFactory
						.createInterpreterStatus(context,
								IInterpreterStatus.ERROR, message);
				status.add(newStatus);
			}
		}
		return null;
	}

	@Override
	public Object evaluate(final EObject context, final String expression)
			throws EvaluationException {
		final String absoluteExpr = FacetOCLUtils.getExpressionWithoutPrefix(expression);
		Object result = null;
		final OCL<?, EClassifier, EOperation, EStructuralFeature, ?, ?, ?, ?, ?, Constraint, ?, ?> ocl = FacetOCLUtils
				.getFacetOCL(context);
		try {
			final OCLExpression<EClassifier> oclExpression = FacetOCLUtils
					.getOclExpression(context, absoluteExpr, ocl);
			result = ocl.evaluate(context, oclExpression);
		} catch (ParserException exc) {
			final String msg = String.format(
					"Error on evaluate expression %s with context %s", //$NON-NLS-1$
					expression, context);
			throw new EvaluationException(msg, exc);
		}
		return result;
	}

	@Override
	public boolean evaluateBoolean(final EObject context,
			final String expression) throws EvaluationException {
		boolean result = false;
		final Object evaluated = evaluate(context, expression);
		if (evaluated instanceof Boolean) {
			result = ((Boolean) evaluated).booleanValue();
		} else {
			final String evaluatedStr = evaluated.toString();
			result = Boolean.valueOf(evaluatedStr).booleanValue();
		}
		return result;
	}

	@Override
	public Collection<EObject> evaluateCollection(final EObject context,
			final String expression) throws EvaluationException {
		Collection<EObject> result = null;
		final Object evaluated = evaluate(context, expression);
		if (evaluated instanceof Collection) {
			@SuppressWarnings("unchecked")
			final Collection<EObject> evaluateds = (Collection<EObject>) evaluated;
			result = evaluateds;
		}
		return result;
	}

	@Override
	public EObject evaluateEObject(final EObject context,
			final String expression) throws EvaluationException {
		EObject result = null;
		final Object evaluated = evaluate(context, expression);
		if (evaluated instanceof EObject) {
			result = (EObject) evaluated;
		}
		return result;
	}

	@Override
	public Integer evaluateInteger(final EObject context,
			final String expression) throws EvaluationException {
		Integer result = null;
		final Object evaluated = evaluate(context, expression);
		if (evaluated instanceof Integer) {
			result = ((Integer) evaluated);
		} else {
			final String evaluatedStr = evaluated.toString();
			result = Integer.valueOf(evaluatedStr);
		}
		return result;
	}

	@Override
	public String evaluateString(final EObject context, final String expression)
			throws EvaluationException {
		String result = null;
		final Object evaluated = evaluate(context, expression);
		if (evaluated instanceof String) {
			result = ((String) evaluated);
		} else {
			result = String.valueOf(evaluated);
		}
		return result;
	}

}
