/**
 * Copyright (c) 2018 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal.interpreter;

import org.eclipse.sirius.common.tools.api.interpreter.IInterpreter;
import org.eclipse.sirius.common.tools.api.interpreter.IInterpreterProvider;

public class FacetOCLInterpreterProvider implements IInterpreterProvider {

	public IInterpreter createInterpreter() {
		return new FacetOCLInterpreter();
	}

	public boolean provides(final String expression) {
		boolean result = false;
		if (expression != null) {
			result = expression.startsWith(FacetOCLInterpreter.PREFIX);
		}
		return result;
	}

}
