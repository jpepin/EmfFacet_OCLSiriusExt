/**
 * Copyright (c) 2018 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.facet.efacet.core.IFacetManager;
import org.eclipse.emf.facet.efacet.core.IFacetManagerFactory;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManagerListener;

public class FacetManagerSessionManagerListener extends SessionManagerListener.Stub {

	@Override
	public void notifyAddSession(final Session newSession) {
		final Resource sessionResource = newSession.getSessionResource();
		final ResourceSet resourceSet = sessionResource.getResourceSet();
		final IFacetManager facetManager = IFacetManagerFactory.DEFAULT.createFacetManager(resourceSet);
		System.out.println(facetManager);
	}

}
