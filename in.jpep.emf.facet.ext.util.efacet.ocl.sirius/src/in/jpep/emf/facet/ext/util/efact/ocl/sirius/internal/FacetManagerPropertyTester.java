/**
 * Copyright (c) 2018 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal;

import org.eclipse.core.expressions.PropertyTester;

import in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal.ui.FacetHandlerUtils;

public class FacetManagerPropertyTester extends PropertyTester {

	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		return FacetHandlerUtils.getFacetManager() != null;
	}

}
