/**
 * Copyright (c) 2018 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal.interpreter;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.sirius.common.tools.api.interpreter.EvaluationException;
import org.eclipse.sirius.common.tools.api.interpreter.IInterpreter;
import org.eclipse.sirius.common.tools.api.interpreter.IInterpreterContext;
import org.eclipse.sirius.common.tools.api.interpreter.IInterpreterStatus;
import org.eclipse.sirius.common.tools.api.interpreter.IVariableStatusListener;
import org.eclipse.sirius.ecore.extender.business.api.accessor.MetamodelDescriptor;
import org.eclipse.sirius.ecore.extender.business.api.accessor.ModelAccessor;

public class InterpreterAdapter implements IInterpreter {

	public boolean provides(String expression) {
		return false;
	}

	public boolean supportsValidation() {
		return false;
	}

	public Collection<IInterpreterStatus> validateExpression(
			IInterpreterContext context, String expression) {
		return null;
	}

	public Collection<EObject> evaluateCollection(EObject context,
			String expression) throws EvaluationException {
		return null;
	}

	public Object evaluate(EObject target, String expression)
			throws EvaluationException {
		return null;
	}

	public boolean evaluateBoolean(EObject context, String expression)
			throws EvaluationException {
		return false;
	}

	public EObject evaluateEObject(EObject context, String expression)
			throws EvaluationException {
		return null;
	}

	public String evaluateString(EObject context, String expression)
			throws EvaluationException {
		return null;
	}

	public Integer evaluateInteger(EObject context, String expression)
			throws EvaluationException {
		return null;
	}

	public void clearImports() {
		// do nothing
	}

	public void addImport(String dependency) {
		// do nothing
	}

	public void setProperty(Object key, Object value) {
		// do nothing
	}

	public void setVariable(String name, Object value) {
		// do nothing
	}

	public void unSetVariable(String name) {
		// do nothing
	}

	public Object getVariable(String name) {
		return null;
	}

	public void clearVariables() {
		// do nothing
	}

	public void dispose() {
		// do nothing
	}

	public void addVariableStatusListener(IVariableStatusListener newListener) {
		// do nothing
	}

	public void removeVariableStatusListener(IVariableStatusListener listener) {
		// do nothing
	}

	public Map<String, ?> getVariables() {
		return null;
	}

	public void setModelAccessor(ModelAccessor modelAccessor) {
		// do nothing
	}

	public String getPrefix() {
		return null;
	}

	public String getVariablePrefix() {
		return null;
	}

	public void setCrossReferencer(ECrossReferenceAdapter crossReferencer) {
		// do nothing
	}

	public Collection<String> getImports() {
		return null;
	}

	public void removeImport(String dependency) {
		// do nothing
	}

	public void activateMetamodels(Collection<MetamodelDescriptor> metamodels) {
		// do nothing
	}

}
