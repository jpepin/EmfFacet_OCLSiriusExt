/**
 * Copyright (c) 2018 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.facet.efacet.core.FacetUtils;
import org.eclipse.emf.facet.efacet.core.IFacetManager;
import org.eclipse.emf.facet.efacet.core.IFacetManagerFactory;
import org.eclipse.emf.facet.efacet.core.IFacetSetCatalogManager;
import org.eclipse.emf.facet.efacet.core.IFacetSetCatalogManagerFactory;
import org.eclipse.emf.facet.efacet.metamodel.v0_2_0.efacet.FacetSet;
import org.eclipse.emf.facet.util.efacet.ocl.core.IEFacetOCLEnvironmentFactoryFactory;
import org.eclipse.ocl.EnvironmentFactory;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.helper.Choice;
import org.eclipse.ocl.helper.ConstraintKind;
import org.eclipse.ocl.helper.OCLHelper;
import org.eclipse.sirius.common.tools.api.contentassist.ContentContext;
import org.eclipse.sirius.common.tools.api.contentassist.ContentInstanceContext;
import org.eclipse.sirius.common.tools.api.contentassist.ContentProposal;
import org.eclipse.sirius.common.tools.api.contentassist.IProposalProvider;
import org.eclipse.sirius.common.tools.api.interpreter.IInterpreter;
import org.eclipse.sirius.common.tools.api.interpreter.IInterpreterContext;
import org.eclipse.sirius.common.tools.api.interpreter.TypeName;
import org.eclipse.sirius.common.tools.api.interpreter.VariableType;

import in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal.interpreter.FacetOCLInterpreter;

public class FacetOCLProposalProvider implements IProposalProvider {

	public ContentProposal getNewEmtpyExpression() {
		return new ContentProposal(FacetOCLInterpreter.PREFIX, FacetOCLInterpreter.PREFIX,
				"OCL query with Facet feature compatibility");
	}

	public List<ContentProposal> getProposals(IInterpreter interpreter, ContentContext context) {
		final EClassifier classifier = getClassifierFromContext(context);
		final ResourceSetImpl resourceSet = new ResourceSetImpl();
		final IFacetManager facetManager = IFacetManagerFactory.DEFAULT.getOrCreateDefaultFacetManager(resourceSet);
		setFacetManaged(classifier, facetManager, resourceSet);
		final EnvironmentFactory facetOCLEnvFactory = IEFacetOCLEnvironmentFactoryFactory.DEFAULT
				.createEFacetOCLEnvironmentFactory(facetManager);
		final OCL ocl = OCL.newInstance(facetOCLEnvFactory);
		ocl.getEvaluationEnvironment();
		final OCLHelper oclHelper = ocl.createOCLHelper();
		oclHelper.setContext(classifier);
		final String txt = FacetOCLUtils.getExpressionWithoutPrefix(context.getContents());
		final List<Choice> oclChoices = oclHelper.getSyntaxHelp(ConstraintKind.INVARIANT, txt);
		return oclChoicesToContentProposal(oclChoices);
	}

	static EClassifier getClassifierFromContext(final ContentContext context) {
		final IInterpreterContext interpreterContext = context.getInterpreterContext();
		final VariableType targetType = interpreterContext.getTargetType();
		final Collection<EPackage> availableEPackages = interpreterContext.getAvailableEPackages();
		final TypeName commonType = targetType.getCommonType(availableEPackages);
		final Collection<EClassifier> classifiers = commonType.search(availableEPackages);
		return classifiers.iterator().next();
	}

	private static void setFacetManaged(final EClassifier classifier, final IFacetManager facetManager,
			final ResourceSetImpl resourceSet) {
		final IFacetSetCatalogManager catalog = IFacetSetCatalogManagerFactory.DEFAULT
				.getOrCreateFacetSetCatalogManager(resourceSet);
		final Collection<FacetSet> facetSets = catalog.getRegisteredFacetSets();
		for (FacetSet facetSet : facetSets) {
			final EPackage extendedEPackage = FacetUtils.getExtendedEPackage(facetSet);
			if (extendedEPackage != null && extendedEPackage.equals(classifier.getEPackage())) {
				facetManager.getManagedFacetSets().add(facetSet);
			}
		}
	}

	public List<ContentProposal> getProposals(IInterpreter interpreter, ContentInstanceContext context) {
		final List<Choice> oclChoices = getOCLChoices(context);
		return oclChoicesToContentProposal(oclChoices);
	}

	private static List<Choice> getOCLChoices(final ContentInstanceContext instanceContext) {
		List<Choice> result = Collections.emptyList();
		final EObject context = instanceContext.getCurrentSelected();
		if (context != null) {
			try {
				final int cursorPosition = instanceContext.getCursorPosition();
				final String console = instanceContext.getTextSoFar();
				final String text = console.substring(0, cursorPosition);
				final OCL<?, EClassifier, EOperation, EStructuralFeature, ?, ?, ?, ?, ?, Constraint, ?, ?> facetOCL = FacetOCLUtils
						.getFacetOCL(context);
				final OCLHelper<EClassifier, EOperation, EStructuralFeature, Constraint> oclHelper = FacetOCLUtils
						.getOclHelper(context, facetOCL);
				result = oclHelper.getSyntaxHelp(ConstraintKind.INVARIANT, text);
			} catch (Exception exc) {
				// TODO Logger.logError(e, Activator.getDefault());
			}
		}
		return result;
	}

	private static List<ContentProposal> oclChoicesToContentProposal(final List<Choice> choices) {
		final List<ContentProposal> proposals = new ArrayList<ContentProposal>(choices.size());
		for (final Choice choice : choices) {
			String information = choice.getDescription();
			String proposal = choice.getName();
			int cursorPosition;
			switch (choice.getKind()) {
			case OPERATION:
			case SIGNAL:
				// the description is already complete
				proposal = proposal + "()"; //$NON-NLS-1$
				information = choice.getDescription();
				cursorPosition = proposal.length() - 1; // between the ()
				break;
			case PROPERTY:
			case ENUMERATION_LITERAL:
			case VARIABLE:
			case ASSOCIATION_CLASS:
				information = proposal + " : " + choice.getDescription(); //$NON-NLS-1$
				cursorPosition = proposal.length();
				break;
			default:
				information = proposal;
				cursorPosition = proposal.length();
				break;
			}
			final ContentProposal contentProposal = new ContentProposal(proposal, information, information,
					cursorPosition);
			proposals.add(contentProposal);
		}
		return proposals;
	}

}
