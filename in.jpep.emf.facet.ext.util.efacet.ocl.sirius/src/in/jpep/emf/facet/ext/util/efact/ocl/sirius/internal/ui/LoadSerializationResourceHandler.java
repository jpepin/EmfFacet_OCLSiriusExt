/**
 * Copyright (c) 2018 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal.ui;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.ui.action.LoadResourceAction.LoadResourceDialog;
import org.eclipse.emf.facet.efacet.core.IFacetManager;
import org.eclipse.emf.facet.efacet.core.exception.FacetManagerException;
import org.eclipse.emf.facet.util.ui.internal.exported.dialog.IOkDialogFactory;
import org.eclipse.emf.facet.util.ui.internal.exported.handler.HandlerUtils;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;

public class LoadSerializationResourceHandler extends AbstractHandler {

	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IFacetManager facetManager = FacetHandlerUtils.getFacetManager();
		if (facetManager != null) {
			final IWorkbenchPart activePart = HandlerUtils.getActivePart();
			final IWorkbenchPartSite site = activePart.getSite();
			final Shell shell = site.getShell();
			final LoadResourceDialog dialog = new LoadSerializationDialog(shell, facetManager);
			dialog.open();
		}
		return null;
	}

	class LoadSerializationDialog extends LoadResourceDialog {

		private final IFacetManager facetManager;

		public LoadSerializationDialog(final Shell parent, final IFacetManager facetManager) {
			super(parent);
			this.facetManager = facetManager;
		}

		@Override
		protected Control createDialogArea(Composite parent) {
			final Control createdDialogArea = super.createDialogArea(parent);
			final URI serialRsrcUri = getSerialResourceUri(this.facetManager);
			if (serialRsrcUri != null) {
				this.uriField.setText(serialRsrcUri.toString());
			}
			return createdDialogArea;
		}

		@Override
		protected boolean processResources() {
			final List<URI> urIs = getURIs();
			final URI serialUri = urIs.get(0);
			changeSerializationURI(this.facetManager, serialUri);
			return true;
		}

	}

	protected static URI getSerialResourceUri(final IFacetManager facetManager) {
		final Resource serialResource = facetManager.getSerializationResource();
		URI uri = null;
		if (serialResource != null) {
			uri = serialResource.getURI();
		}
		return uri;
	}

	protected static void changeSerializationURI(final IFacetManager facetManager, final URI uri) {
		Resource sResource = null;
		final ResourceSet resourceSet = facetManager.getResourceSet();
		try {
			sResource = resourceSet.getResource(uri, true);
		} catch (@SuppressWarnings("unused") Exception exc) {
			/*
			 * @SuppressWarnings("unused") > jpepin, catched by creation instead of loading
			 */
			sResource = resourceSet.createResource(uri);
		}
		try {
			facetManager.setSerializationResource(sResource);
			FacetHandlerUtils.refresh();
		} catch (FacetManagerException e) {
			IOkDialogFactory.DEFAULT.openErrorDialog(new Shell(), e, "Error on change serialization resource"); //$NON-NLS-1$
		}
	}

}