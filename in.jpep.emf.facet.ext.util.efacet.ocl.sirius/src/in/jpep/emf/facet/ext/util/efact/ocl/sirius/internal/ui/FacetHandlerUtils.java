/**
 * Copyright (c) 2018 Jonathan Pepin
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Jonathan Pepin - initial API and implementation
 */
package in.jpep.emf.facet.ext.util.efact.ocl.sirius.internal.ui;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.facet.efacet.core.IFacetManager;
import org.eclipse.emf.facet.util.ui.internal.exported.handler.HandlerUtils;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.diagram.ui.tools.api.editor.DDiagramEditor;
import org.eclipse.ui.IWorkbenchPart;

public final class FacetHandlerUtils {

	private FacetHandlerUtils() {
		// must not be called
	}

	public static IFacetManager getFacetManager() {
		IFacetManager result = null;
		final IWorkbenchPart activePart = HandlerUtils.getActivePart();
		if (activePart instanceof DDiagramEditor) {
			final DDiagramEditor diagramEditor = (DDiagramEditor) activePart;
			final Session session = diagramEditor.getSession();
			final Resource resource = session.getSessionResource();
			final ResourceSet resourceSet = resource.getResourceSet();
			for (Adapter adapter : resourceSet.eAdapters()) {
				if (adapter instanceof IFacetManager) {
					result = (IFacetManager) adapter;
				}
			}
		}
		return result;
	}

	public static void refresh() {
		final IWorkbenchPart activePart = HandlerUtils.getActivePart();
		if (activePart instanceof DDiagramEditor) {
			final DDiagramEditor diagramEditor = (DDiagramEditor) activePart;
			final Session session = diagramEditor.getSession();
			final TransactionalEditingDomain editingDomain = session.getTransactionalEditingDomain();
			final RecordingCommand cmd = new RecordingCommand(editingDomain) {
				@Override
				protected void doExecute() {
					diagramEditor.getRepresentation().refresh();
				}
			};
			editingDomain.getCommandStack().execute(cmd);
		}
	}

}
